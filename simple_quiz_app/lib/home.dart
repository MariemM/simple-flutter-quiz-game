import 'package:flutter/material.dart';
import 'package:simple_quiz_app/custom_widgets/text.dart';
import 'package:simple_quiz_app/question_page.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Quizz Game Flutter'),
      ),
      body: Center(
        child: Card(
          margin: const EdgeInsets.symmetric(horizontal: 30),
          color: Colors.red.shade100,
          elevation: 12,
          child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    'images/cover.jpg',
                    height: size.height / 2.5,
                    fit: BoxFit.cover,
                  ),
                  const Padding(padding: EdgeInsets.all(4)),
                  ElevatedButton(
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext ctx) => Question())),
                    child: TextMedium(text: 'Start Quizz'),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
