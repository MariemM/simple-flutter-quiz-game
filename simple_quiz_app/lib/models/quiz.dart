class Quiz {
  String question;
  String? explication;
  bool answer;
  String imageName;

  Quiz({
    required this.question,
    required this.answer,
    required this.imageName,
    this.explication,
  });

  String getImage() => 'images/$imageName';

  Map<String, dynamic> checkAnswer(bool resp) {
    return {
      'isRight': (answer != resp) ? false : true,
      'explication': explication
    };
  }
}
