import 'package:simple_quiz_app/models/quiz.dart';

class QuizData {
  List<Quiz> quizData = [
    Quiz(
        question: "La devise de la Belgique est l'union fait la force",
        answer: true,
        imageName: 'belgique.jpg'),
    Quiz(
        question: "La lune va finir par tomber sur terre à cause de la gravité",
        answer: false,
        explication: "Au contraire la lune s'éloigne",
        imageName: 'lune.jpg'),
    Quiz(
        question: "La Russie est plus grande en superficie que Pluton",
        answer: true,
        imageName: 'russie.jpg'),
    Quiz(
        question: "Nyctalope est une race naine d'antilope",
        answer: false,
        explication: "C'est une aptitude à voir dans le noir",
        imageName: 'nyctalope.jpg'),
    Quiz(
        question: "Le commodore 64 est l'ordinateur de bureau le plus vendu",
        answer: true,
        imageName: 'commodore.jpg'),
    Quiz(
        question: "Le nom du drapeau des pirates est black skull",
        answer: false,
        explication: "Il s'appelle Jolly Roger",
        imageName: 'pirate.png'),
    Quiz(
        question: "Haddock est le nom du chien de Tintin",
        answer: false,
        explication: "C'est Milou",
        imageName: 'tintin.jpg'),
    Quiz(
        question: "La barbe des pharaons était fausse",
        answer: true,
        explication: "A l'époque d'éja ils utilisaient des postiches",
        imageName: 'pharaon.jpg'),
    Quiz(
        question:
            "Au Québec tire toi une buche veut dire viens viens t'asseoir",
        answer: true,
        explication: "La buche, fameuse chaise de bucheron",
        imageName: 'buche.jpg'),
    Quiz(
        question: "Le module lunaire Eagle posséde de 4Ko de Ram",
        answer: true,
        imageName: 'eagle.jpg'),
  ];
}
