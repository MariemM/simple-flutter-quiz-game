import 'package:flutter/material.dart';
import 'package:simple_quiz_app/custom_widgets/alert_answer.dart';
import 'package:simple_quiz_app/custom_widgets/alert_finish.dart';
import 'package:simple_quiz_app/custom_widgets/text.dart';
import 'package:simple_quiz_app/data/quiz_data.dart';
import 'package:simple_quiz_app/home.dart';
import 'package:simple_quiz_app/models/quiz.dart';

class Question extends StatefulWidget {
  const Question({super.key});
  @override
  State<Question> createState() => _QuestionState();
}

class _QuestionState extends State<Question> {
  List<Quiz> quizList = QuizData().quizData;
  int counter = 0;
  int index = 0;
  @override
  Widget build(BuildContext context) {
    Quiz currentQuiz = quizList[index];
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Score: $counter'),
      ),
      body: Container(
        width: size.width,
        child: Card(
            margin: const EdgeInsets.all(10),
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextSmall(
                    text: 'Question number: ${index + 1}/${quizList.length}',
                    fontStyle: FontStyle.italic,
                    fontColor: Colors.redAccent,
                  ),
                  TextMedium(text: currentQuiz.question),
                  Image.asset(currentQuiz.getImage()),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                        onPressed: (() => onAnswer(false)),
                        child: TextSmall(text: 'False'),
                      ),
                      ElevatedButton(
                        onPressed: (() => onAnswer(true)),
                        style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll<Color>(Colors.green)),
                        child: TextSmall(text: 'True'),
                      )
                    ],
                  )
                ],
              ),
            )),
      ),
    );
  }

  void onAnswer(bool answer) {
    Map<String, dynamic> result = quizList[index].checkAnswer(answer);
    setState(() {
      if (result['isRight'] == true) {
        counter++;
      }
    });
    AnswerAlert alert = AnswerAlert(
        isAnswerRight: result['isRight'],
        explication: result['explication'],
        onPressed: (() => setState(() => onNextQuiz())));

    showAlert(alert: alert);
  }

  void onNextQuiz() {
    setState(() {
      if (index < quizList.length - 1) {
        index++;
        Navigator.of(context).pop();
      } else {
        Navigator.of(context).pop();
        showFinishAlert();
      }
    });
  }

  Future<void> showFinishAlert() async {
    FinishQuizAlert alert = FinishQuizAlert(
        score: counter,
        onPressed: (() {
          Navigator.of(context).pop();
          Navigator.push(context, MaterialPageRoute(builder: (ctx) => Home()));
        }));
    return await showAlert(alert: alert);
  }

  Future<void> showAlert({required Widget alert}) async {
    return await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext ctx) {
          return alert;
        });
  }
}
