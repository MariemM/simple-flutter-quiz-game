import 'package:flutter/material.dart';
import 'package:simple_quiz_app/custom_widgets/text.dart';

class FinishQuizAlert extends StatelessWidget {
  final int score;
  final Function onPressed;

  const FinishQuizAlert(
      {super.key, required this.score, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    String title = score < 10 ? 'Game Over!' : 'Excellent!';
    return AlertDialog(
      title: Center(
        child: TextLarge(text: title),
      ),
      content: TextSmall(text: 'Your score is: $score points'),
      actions: [
        TextButton(onPressed: (() => onPressed()), child: TextSmall(text: 'OK'))
      ],
    );
  }
}
