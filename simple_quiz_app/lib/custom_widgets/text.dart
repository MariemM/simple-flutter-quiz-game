import 'package:flutter/material.dart';

class SimpleText extends Text {
  final String text;
  final Color? fontColor;
  final double? fontSize;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;

  SimpleText(
      {super.key,
      required this.text,
      this.fontColor,
      this.fontSize,
      this.fontWeight,
      this.fontStyle})
      : super(
          text,
          style: TextStyle(
              fontSize: fontSize,
              color: fontColor,
              fontWeight: fontWeight,
              fontStyle: fontStyle),
        );
}

class TextSmall extends SimpleText {
  TextSmall({super.key, required super.text, super.fontColor, super.fontStyle})
      : super(fontSize: 18);
}

class TextMedium extends SimpleText {
  TextMedium({super.key, required super.text})
      : super(
          fontWeight: FontWeight.bold,
          fontSize: 22,
        );
}

class TextLarge extends SimpleText {
  TextLarge({super.key, required super.text})
      : super(
          fontWeight: FontWeight.bold,
          fontSize: 30,
        );
}
