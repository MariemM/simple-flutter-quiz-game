import 'package:flutter/material.dart';
import 'package:simple_quiz_app/custom_widgets/text.dart';

class AnswerAlert extends StatefulWidget {
  final bool isAnswerRight;
  final String? explication;
  final Function onPressed;
  const AnswerAlert(
      {super.key,
      required this.isAnswerRight,
      this.explication,
      required this.onPressed});
  @override
  State<AnswerAlert> createState() => _AnswerAlertState();
}

class _AnswerAlertState extends State<AnswerAlert> {
  @override
  Widget build(BuildContext context) {
    String title = widget.isAnswerRight ? 'Well Done' : 'Oops!, You missed it';
    String imageName = widget.isAnswerRight ? 'well_done.png' : 'wrong.png';
    return SimpleDialog(
      title: Center(
        child: TextLarge(
          text: title,
        ),
      ),
      contentPadding: const EdgeInsets.symmetric(vertical: 20),
      elevation: 10,
      children: [
        Image.asset('images/$imageName'),
        (widget.explication != null)
            ? Padding(
                padding: const EdgeInsets.all(10),
                child: TextSmall(text: widget.explication!))
            : const Padding(padding: EdgeInsets.all(5)),
        Center(
          child: TextButton(
              onPressed: (() {
                widget.onPressed();
              }),
              child: TextSmall(text: 'Next Question')),
        )
      ],
    );
  }
}
